<?php

class Route
{
    private $path;
    private $callable;
    private $matches;
    public function __construct($path, $callable)
    {
        $this->path = trim($path, '/');
        $this->callable = $callable;
    }
    function match($url) {
        $url = trim($url, '/');
        $path = preg_replace('#:([\w]+)#', '([^/]+)', $this->path);
        // var_dump($path);
        $regex = "#^$path$#i";
        // var_dump($regex);
        if (!preg_match($regex, $url, $matches)) {
            return false;
        }
        // var_dump($matches);
        $this->matches = $matches;
        return true;
    }
    public function call()
    {
        return call_user_func_array($this->callable, $this->matches);
    }

}
