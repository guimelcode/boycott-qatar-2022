<?php
require __DIR__ . '/Route.php';
require __DIR__ . '/RouterException.php';
class Router
{

    private $url;
    private $routes = [];
    public function __construct($url)
    {
        $this->url = $url;
    }
    public function get($path, $callable)
    {
        $route = new Route($path, $callable);
        $this->routes['GET'][] = $route;
    }

    public function run()
    {
        // echo '<pre>';
        // echo print_r($this->routes);
        // echo '</pre>';
        if (!isset($this->routes[$_SERVER['REQUEST_METHOD']])) {
            throw new RouterException('REQUEST_METHOD error');
        }

        foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route) {
            if ($route->match($this->url)) {
               return $route->call();
            }
        }

        throw new RouterException('no routes matches');

    }

}
