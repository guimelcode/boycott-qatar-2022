# ALTERNATIVPROGRAMM:

Schalten Sie einfach um, essen Sie mit Ihren Freunden abends zusammen oder veranstalten Sie einen Filmabend. Gehen Sie ins Kino oder ins Theater. Damit zeigen Sie deutlich Ihre Ablehnung gegen das korrumpierte Turnier. Wer nicht auf Fußball verzichten kann, kann sich die großen Spiele der vergangenen Weltmeisterschaften und die Geschichte der eigenen Fußballnation ansehen und in Erinnerung schwelgen. Wir wissen gut, dass gerade jetzt das Fußballfieber wieder steigt, dass uns alle vier Jahre ereilt. Deshalb hier ein Alternativprogramm, um dem schandhaften Turnier in Katar aus dem Weg zu gehen.

In Paris, an alternative daily program of historic World Cup matches will be offered throughout the tournament, at : 

[Babel Café (109 Bd de Ménilmontant, 75011)](https://www.facebook.com/babelcafeparis)

![Babel Café Program](/contenus/imgs/NoQataran-Programme-OKweb.jpg)
[Link to download the Babel Café Program](/contenus/imgs/NoQataran-Programme-OK.pdf){target="_blanck"}

## YOU CAN ALSO WATCH…

## FILMS:

[Offside, Jafar Panahi (trailer)](https://www.youtube.com/watch?v=xs0yPRNjpNw)

[Green Street Hooligans, Lexi Alexander (full movie)](https://www.youtube.com/watch?v=tRz7tFm28OE)

[The Damned United, Tom Hooper (trailer)](https://www.youtube.com/watch?v=Abpj_-fpc28)

[Goal!, Danny Cannon and Michael Winterbottom (full movie)](https://www.youtube.com/watch?v=5UACSTW-qMo)

[Looking for Eric, Ken Loach (trailer)](https://www.youtube.com/watch?v=DFCmF32HSjI)

[Escape To Victory, John Huston (trailer)](https://www.youtube.com/watch?v=MO2yuDWi-IA)

[Bend It Like Beckham, Gurinder Chadha (trailer)](https://www.youtube.com/watch?v=oQb8ShgVjJA)

[United, James Strong (full movie)](https://www.youtube.com/watch?v=GhC-7T4oD00)

[The Miracle of Bern, Sönke Wortmann (full movie, in german)](https://www.youtube.com/watch?v=RmbVjPvobLw)

[Fever Pitch, David Evans (full movie)](https://www.youtube.com/watch?v=yevek3oxUGw)

[La Pena Máxima, Jorge Echeverry (full movie in spanish with english subtitles)](https://www.youtube.com/watch?v=Tj2177Sq15g)

[Pelé, Ben Nicholas and David Tryhorn (trailer, available on Netflix)](https://www.youtube.com/watch?v=YOf8It9LNwc)

[The Hand of God, Paolo Sorrentino (trailer, available on Netflix)](https://www.youtube.com/watch?v=i_1VW_0i6vo)

## DOCUMENTARIES: 

[Maradona by Kusturica, Emir Kusturica (full documentary)](https://soap2day.rs/watch-movie/watch-maradona-by-kusturica-full-67210.5512984)

[The Two Escobars, Zimbalist brothers (full documentary)](https://watchdocumentaries.com/the-two-escobars/)

[Next Goal Wins, Mike Brett and Steve Jamison (full documentary)](https://clonized.monster/movies/play/next-goal-wins-2014?mid=17&sid=&sec=61207d9d4dfe1fd6de1f7cf2c777cf0225942bf2&t=1668370881)

[Nossa Chape, Zimbalist Brothers (full documentary)](https://m4uhd.tv/watch-movie-nossa-chape-2018-231193.html)

[Hillsborough, Daniel Gordon (full documentary)](https://watchdocumentaries.com/hillsborough/)

[50 Most shocking moments in World Cup history, BBC (full)](https://www.youtube.com/watch?v=N3v89MzANqU)

[Diego Maradona, Asif Kapadia (full documentary)](https://www.youtube.com/watch?v=yMQyQRw1Bbc)

## SERIES: 

Ted Lasso (available on Apple TV)

Baggio: The Divine Ponytail (available on Netflix)

The English Game (available on Netflix)

Club de Cuervos (available on Netflix)

Goles en Contra (available on Netflix)

## DOCUMENTARY SERIES:

Sunderland 'Til I Die (available on Netflix)

The Figo Affair: The Transfer that Changed Football (available on Netflix)

Captains (available on Netflix)

Becoming Champions (available on Netflix)

FIFA Uncovered (available on Netflix)
 