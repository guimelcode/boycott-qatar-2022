# VERBREITEN SIE DAS POSTER UND DIE BOYCOTTKAMPAGNE:

Unterstützen Sie die Kampagne und teilen Sie sie online!
Online und druckfähige Versionen der Kampagnenposter stehen hier zum Download bereit.
Das Poster und/oder der Sticker ist ebenfalls erhältlich.
+ Paris (FR) :
    + [librairie Le Monte-en-l'air (2 Rue de la Mare, 75020)](https://montenlair.fr/){target="_blanck"}
    + [bar-restaurant Le Lieu-dit (6 rue Sorbier, 75020)](https://lelieudit.com/){target="_blanck"}
+ Ivry (FR) :
    + [librairie Envie de lire (16 Rue Gabriel Péri, 94200)](https://www.facebook.com/Librairie.Envie.de.lire/){target="_blanck"}
+ Heidelberg (DE) :
    + [Der Keramikofen (Untere Str. 12, 69117 Heidelberg)](https://www.derkeramikofen.de/){target="_blanck"}
+ Karlsruhe (DE) :
    + [P8, Schauenburgstraße 5, 76135 Karlsruhe](https://www.p-acht.org/){target="_blanck"}


Wenn Sie Transport oder Verteilung der Poster und Sticker in anderen Städten organisieren können, zögern Sie bitte nicht, uns hier zu erreichen:
[boycott-qatar22@proton.me](mailto:boycott-qatar22@proton.me) 


![Poster boycott Qatar 2022, web format ](/contenus/imgs/BoycottQatar2022-web.jpg)
+ [Link to download the A3 format poster](/contenus/imgs/BoycottQatar2022-A3.pdf){target="_blanck"}
+ [Link to download the A4 format poster ](/contenus/imgs/BoycottQatar2022-A4.pdf){target="_blanck"}
+ [Link to download the web format poster ](/contenus/imgs/BoycottQatar2022-web.jpg){target="_blanck"}

![Poster boycott Qatar 2022,alias format ](/contenus/imgs/BoycottQatar2022-alias.jpg)
+ [Link to download the alias format poster ](/contenus/imgs/BoycottQatar2022-alias.jpg){target="_blanck"} 