# UNTERSTÜTZEN SIE DIE KAMPAGNE MIT EINER SPENDE

Sie können diese Boycottkampagne mit einer Spende der Höhe Ihrer Wahl unterstützen:

*[link to come]*

Erstens wird uns dies helfen, für die Kosten für Sticker und Poster aufzukommen. Zweitens und noch wichtiger: Überschüssiges Geld wird an die NGO XXXXXXX gehen, die die Familien der Arbeitsmigranten, die auf den Baustellen der Weltmeisterschaft in Katar umgekommen sind, unterstützt.