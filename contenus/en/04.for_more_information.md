# FOR MORE INFORMATION

Stay informed and spread the word, here you will find links about the criminal institution and the businessmen directing it, as well as  the scandals involving the FIFA World Cup Qatar 2022:


## Articles:

[Revealed: 6,500 migrant workers have died in Qatar since World Cup awarded, The Guardian](https://www.theguardian.com/global-development/2021/feb/23/revealed-migrant-worker-deaths-qatar-fifa-world-cup-2022)

[Qatar World Cup of Shame, Amnesty International](https://www.amnesty.org/en/latest/campaigns/2016/03/qatar-world-cup-of-shame/)

[Qatar: Failure to investigate migrant worker deaths leaves families in despair, Amnesty international](https://www.amnesty.org/en/latest/news/2021/08/qatar-failure-to-investigate-migrant-worker-deaths-leaves-families-in-despair/)

[Qatar, Human Rights Watch](https://www.hrw.org/middle-east/north-africa/qatar)

[U.S. Says FIFA Officials Were Bribed to Award World Cups to Russia and Qatar, The New York Times](https://www.nytimes.com/2020/04/06/sports/soccer/qatar-and-russia-bribery-world-cup-fifa.html)


## Videos:

[Working Conditions in Qatar Ahead of the FIFA World Cup 2022, Amnesty International](https://www.youtube.com/watch?v=kiMWl3wE9VI)

[FIFA Uncovered, Netflix documentary](https://www.youtube.com/watch?v=V0UlWZNp6cI)

[FIFA and the World Cup: Last Week Tonight with John Oliver (HBO)](https://www.youtube.com/watch?v=DlJEt2KU33I)

[FIFA II: Last Week Tonight with John Oliver (HBO)](https://www.youtube.com/watch?v=qr6ar3xJL_Q)

[WHY we all take part after all | Qatar 2022, DW Kick off!](https://www.youtube.com/watch?v=8I2SqSi_EGs&list=PLCCm223XGWWgtqzMKvcpmkkVHeYBkou5-&index=1)

[Soccer World Cup: Migrant laborers in Qatar | DW Documentary](https://www.youtube.com/watch?v=q4syhqpFzfM)

[WHY rich regimes are ruining football | Newcastle, Man City, PSG and Bayern Munich, DW Kick off!](https://www.youtube.com/watch?v=OMTszkzVSx4&list=PLeEtWQYUQ8QdZLq0TTlCq95sETSog_4IW&index=34)

[Qatar: World Cup 2022 forced labour, Amnesty International](https://www.youtube.com/watch?v=BCzEJvH0p5)