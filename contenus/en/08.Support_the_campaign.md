# SUPPORT THE CAMPAIGN WITH A DONATION

You can support this boycott campaign by making a donation of an amount of your liking:

*[link to come]*

First, this will allow us to reimburse the costs incurred for the printing of the posters and stickers. Second and more importantly, the remaining money will go to the NGO […] which supports the families of migrant workers who have died on the construction sites of the World Cup in Qatar.