# CONTACT US

We are more than willing to work together with other campaigns, counter-events, movements and different types of protest against modern football, FIFA, and specially Qatar 2022.
For inquires and more, please reach us under the following email address:
[boycott-qatar22@proton.me](mailto:boycott-qatar22@proton.me)