# PROGRAMMATION ALTERNATIVE

Il suffit de changer de chaîne, de dîner avec vos amis ou de faire une soirée cinéma pour montrer votre malaise à l'égard d'un tournoi hors de contrôle. Ou bien de regarder vos matchs préférés des Coupes du monde précédentes, de ce siècle ou du siècle dernier : revoyez ces matchs légendaires qui font l'âme du football avec vos amis et votre famille, suivez votre équipe au fil des coupes précédentes. Nous comprenons que la fièvre qui nous poursuit depuis quatre ans est maintenant à son comble, c'est pourquoi nous proposons des contre-programmations alternatives à cette Coupe de la honte.

À Paris, une programmation alternative quotidienne de matchs historiques de la Coupe du monde sera proposée durant tout le tournoi, au :

[Babel Café (109 Bd de Ménilmontant, 75011)](https://www.facebook.com/babelcafeparis)

![Programme du Babel Café](/contenus/imgs/NoQataran-Programme-OKweb.jpg)
[Lien pour télécharger le programme du Babel Café ](/contenus/imgs/NoQataran-Programme-OK.pdf){target="_blanck"}

## VOUS POUVEZ AUSSI REGARDER…

## FILMS :

[Offside, Jafar Panahi (trailer)](https://www.youtube.com/watch?v=xs0yPRNjpNw)

[Green Street Hooligans, Lexi Alexander (film complet)](https://www.youtube.com/watch?v=tRz7tFm28OE)

[The Damned United, Tom Hooper (trailer)](https://www.youtube.com/watch?v=Abpj_-fpc28)

[Goal!, Danny Cannon and Michael Winterbottom (film complet)](https://www.youtube.com/watch?v=5UACSTW-qMo)

[Looking for Eric, Ken Loach (trailer)](https://www.youtube.com/watch?v=DFCmF32HSjI)

[Escape To Victory, John Huston (trailer)](https://www.youtube.com/watch?v=MO2yuDWi-IA)

[Bend It Like Beckham, Gurinder Chadha (trailer)](https://www.youtube.com/watch?v=oQb8ShgVjJA)

[United, James Strong (film complet)](https://www.youtube.com/watch?v=GhC-7T4oD00)

[The Miracle of Bern, Sönke Wortmann (film complet, en allemand)](https://www.youtube.com/watch?v=RmbVjPvobLw)

[Fever Pitch, David Evans (film complet)](https://www.youtube.com/watch?v=yevek3oxUGw)

[La Pena Máxima, Jorge Echeverry (full movie in spanish with english subtitles)](https://www.youtube.com/watch?v=Tj2177Sq15g)

[Pelé, Ben Nicholas and David Tryhorn (trailer, disponible sur Netflix)](https://www.youtube.com/watch?v=YOf8It9LNwc)

[The Hand of God, Paolo Sorrentino (trailer, disponible sur Netflix)](https://www.youtube.com/watch?v=i_1VW_0i6vo)

## DOCUMENTAIRES : 

[Maradona by Kusturica, Emir Kusturica (film complet)](https://soap2day.rs/watch-movie/watch-maradona-by-kusturica-full-67210.5512984)

[The Two Escobars, Zimbalist brothers (film complet)](https://watchdocumentaries.com/the-two-escobars/)

[Next Goal Wins, Mike Brett and Steve Jamison (film complet)](https://clonized.monster/movies/play/next-goal-wins-2014?mid=17&sid=&sec=61207d9d4dfe1fd6de1f7cf2c777cf0225942bf2&t=1668370881)

[Nossa Chape, Zimbalist Brothers (film complet)](https://m4uhd.tv/watch-movie-nossa-chape-2018-231193.html)

[Hillsborough, Daniel Gordon (film complet)](https://watchdocumentaries.com/hillsborough/)

[50 Most shocking moments in World Cup history, BBC (film complet)](https://www.youtube.com/watch?v=N3v89MzANqU)

[Diego Maradona, Asif Kapadia (film complet)](https://www.youtube.com/watch?v=yMQyQRw1Bbc)

## SÉRIES : 

Ted Lasso (disponible sur Apple TV)

Baggio: The Divine Ponytail (disponible sur Netflix)

The English Game (disponible sur Netflix)

Club de Cuervos (disponible sur Netflix)

Goles en Contra (disponible sur Netflix)

## SÉRIES DOCUMENTAIRES :

Sunderland 'Til I Die (disponible sur Netflix)

The Figo Affair: The Transfer that Changed Football (disponible sur Netflix)

Captains (disponible sur Netflix)

Becoming Champions (disponible sur Netflix)

FIFA Uncovered (disponible sur Netflix)

 