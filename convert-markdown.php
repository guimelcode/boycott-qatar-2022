<?php

use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\Attributes\AttributesExtension;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\SmartPunct\SmartPunctExtension;
use League\CommonMark\Extension\Footnote\FootnoteExtension;
use League\CommonMark\Extension\FrontMatter\FrontMatterExtension;
use League\CommonMark\Extension\FrontMatter\Output\RenderedContentWithFrontMatter;

use League\CommonMark\MarkdownConverter;

class ConvertMarkdown
{
    protected array $config = [
        'smartpunct' => [
            'double_quote_opener' => '« ',
            'double_quote_closer' => ' »',
            'single_quote_opener' => '‹ ',
            'single_quote_closer' => ' ›',
        ],
        'footnote' => [
            'backref_class' => 'footnote-backref',
            'backref_symbol' => '↩',
            'container_add_hr' => false,
            'container_class' => 'footnotes',
            'ref_class' => 'footnote-ref',
            'ref_id_prefix' => 'fnref:',
            'footnote_class' => 'footnote',
            'footnote_id_prefix' => 'fn:',
        ],
    ];
    protected $environment = null;
    protected $converter = null;
    public function __construct()
    {
        // print_r($this->config) ;
        $this->environment = new Environment($this->config);
        $this->environment->addExtension(new CommonMarkCoreExtension());
        $this->environment->addExtension(new AttributesExtension());
        $this->environment->addExtension(new SmartPunctExtension());
        $this->environment->addExtension(new FootnoteExtension());
        $this->environment->addExtension(new FrontMatterExtension());

        $this->converter = new MarkdownConverter($this->environment);
    }

    /**
     * convert
     *
     * @param  string $path
     * @return array
     */
    public function convert(string $path)
    {
        // echo $path . PHP_EOL;

        $fromMarkdown = $this->converter->convert(file_get_contents($path));

        if ($fromMarkdown instanceof RenderedContentWithFrontMatter) {
            $frontMatter = $fromMarkdown->getFrontMatter();
           // print_r($frontMatter);
            return array(
                'layout' => array_key_exists( 'layout', $frontMatter)? $frontMatter['layout'] : 'default',
                'id' => array_key_exists( 'id', $frontMatter)  ? $frontMatter['id']: '',
                'html' => $fromMarkdown->getContent(),
            );
        } else {
            return array(
                'layout' =>  'default',
                'id' =>  '',
                'html' => $fromMarkdown->getContent(),
            );
            
        }



    }

}
