<?php

require __DIR__ . '/vendor/autoload.php';
require_once './convert-markdown.php';

function recurseCopy(
    string $sourceDirectory,
    string $destinationDirectory,
    string $childFolder = ''
): void {
    $directory = opendir($sourceDirectory);

    if (is_dir($destinationDirectory) === false) {
        mkdir($destinationDirectory);
    }

    if ($childFolder !== '') {
        if (is_dir("$destinationDirectory/$childFolder") === false) {
            mkdir("$destinationDirectory/$childFolder");
        }

        while (($file = readdir($directory)) !== false) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            if (is_dir("$sourceDirectory/$file") === true) {
                recurseCopy("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file");
            } else {
                copy("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file");
            }
        }

        closedir($directory);

        return;
    }

    while (($file = readdir($directory)) !== false) {
        if ($file === '.' || $file === '..') {
            continue;
        }

        if (is_dir("$sourceDirectory/$file") === true) {
            recurseCopy("$sourceDirectory/$file", "$destinationDirectory/$file");
        } else {
            copy("$sourceDirectory/$file", "$destinationDirectory/$file");
        }
    }

    closedir($directory);
}

class GenerateStatic
{
    public function init()
    {

        echo 'En cours… ' . PHP_EOL;

        // Récupérer les fichiers contenu
        $contentDir = __DIR__ . '/contenus';
        $contentFiles = array_diff(scandir($contentDir), array('.', '..', 'imgs'));

        $md = new ConvertMarkdown();


        $rendered  = $this->render_part('/parts/header.php');
        foreach ($contentFiles as $file) {
            echo 'Voici un fichier' . PHP_EOL;
            $data = $md->convert($contentDir . '/' . $file);
            print_r($data);
            //    echo $twig->render('default.html.twig', ['content' => $data['html']]);

            $outputDir = __DIR__ . '/dist';
            $outputPath = $outputDir . '/index.html';
            // $rendered .= $data['html'];
            $rendered .= $this->render_template(array(
                'content' => $data['html'],
                'template' => $data['layout'],
                'id' => $data['id'],
            ));
        }
        $rendered  .= $this->render_part('/parts/footer.php');
        file_put_contents($outputPath, $rendered);
        recurseCopy(__DIR__ . '/contenus/imgs', __DIR__ . '/dist', 'imgs');
        recurseCopy(__DIR__ . '/css', __DIR__ . '/dist', 'css');

    }
    public function render_part($path)
    {
        ob_start();
        include __DIR__ . $path;
        return ob_get_clean();
    }

    public function render_template($locals = array())
    {
        extract($locals, EXTR_SKIP);
        ob_start();
        include __DIR__ . '/templates/' . $template . '.tpl.php';
        // echo 'coucouc-------------';
        return ob_get_clean();
        // return ob_end_flush();
    }
}

$generate = new GenerateStatic();

$generate->init();
