<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/Router/Router.php';
require __DIR__ . '/render.php';

// include __DIR__ . '/parts/header.php';

// print_r($_GET);

// die($_GET['url']);

$router = new Router($_GET['url']);

$router->get('/', function () {render('en');});
$router->get('/en', function () {render('en');});
$router->get('/fr', function () {render('fr');});
$router->get('/de', function () {render('de');});
$router->get('/es', function () {render('es');});

try {
    $router->run();
    //code...
} catch (\Throwable$th) {
    //throw $th;
    echo 'Exception reçue lol: ', $th->getMessage(), "\n";
}

?>



<?php
include __DIR__ . '/parts/footer.php';
?>