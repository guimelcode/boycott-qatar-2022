class TemplateSession {
  section = null;
  title = null;
  toClose = null;
  state = {
    open: 0,
  };
  timeoutResize = null;
  constructor(_section) {
    this.section = _section;
    this.title = _section.getElementsByTagName("h1")[0];
    this.toClose = _section.getElementsByClassName("to-close")[0];
    this.stateManage();
    window.addEventListener(
      "resize",
      () => {
        clearTimeout(this.timeoutResize);
        this.timeoutResize = setTimeout(() => {
        //   console.log("lol");
          this.stateManage();
        }, 250);
      }
    );
  }
  stateManage = () => {
    if (!this.state.open) {
      this.setSectionHeight(this.getTitleHeight());
      this.removeToCloseClick();
      this.addTitleClick();
    } else {
      this.setSectionHeight(this.getSectionHeight());
      this.removeTitleClick();
      this.addToCloseClick();
    }
  };
  addTitleClick = () => {
    // this.title.style.cursor = "pointer";
    this.title.classList.add("clickable");
    this.title.addEventListener("click", this.handleTitleClick);
  };

  removeTitleClick = () => {
    // this.title.style.cursor = "unset";
    this.title.classList.remove("clickable");

    this.title.removeEventListener("click", this.handleTitleClick);
  };

  handleTitleClick = () => {
    this.state.open = 1;
    this.stateManage();
  };

  addToCloseClick = () => {
    this.toClose.addEventListener("click", this.handleToCloseClick);
  };
  removeToCloseClick = () => {
    this.toClose.removeEventListener("click", this.handleToCloseClick);
  };
  handleToCloseClick = () => {
    this.state.open = 0;
    this.stateManage();
  };

  getTitleHeight = () => {
    const h1Height = this.title.offsetHeight + "px";
    const h1MarginTop = window
      .getComputedStyle(this.title)
      .getPropertyValue("margin-top");
    const h1Marginbottom = window
      .getComputedStyle(this.title)
      .getPropertyValue("margin-bottom");
    return `calc(${h1Height} + ${h1MarginTop} + ${h1Marginbottom})`;
  };
  getSectionHeight = () => {
    let childsHeight = 0;
    let childsMarginTop = "";
    let childsMarginTBottom = "";

    for (let index = 0; index < this.section.children.length; index++) {
      const child = this.section.children[index];
      childsHeight += child.offsetHeight;
      if (index !== this.section.children.length - 1) {
        childsMarginTop += ` ${window
          .getComputedStyle(child)
          .getPropertyValue("margin-top")} +`;
        childsMarginTBottom += ` ${window
          .getComputedStyle(child)
          .getPropertyValue("margin-bottom")} +`;
      } else {
        childsMarginTop += ` ${window
          .getComputedStyle(child)
          .getPropertyValue("margin-top")}`;
        childsMarginTBottom += ` ${window
          .getComputedStyle(child)
          .getPropertyValue("margin-bottom")}`;
      }
    }

    return `calc(${childsHeight}px + ${childsMarginTop} + ${childsMarginTBottom})`;
  };
  setSectionHeight = (_height) => (this.section.style.maxHeight = _height);
}

window.onload = () => {
  const sections = document.getElementsByClassName("tpl-default");
  const sectionCollection = [];
  for (let i = 0; i < sections.length; i++) {
    sectionCollection[i] = new TemplateSession(sections[i]);
    // const firstTitle = sections[i].getElementsByTagName("h1");

    // sections[i].style.maxHeight = firstTitle[0].offsetHeight + "px";

    // sections[i].style.maxHeight = `calc(${
    //   firstTitle[0].offsetHeight
    // }px + ${window
    //   .getComputedStyle(firstTitle[0])
    //   .getPropertyValue("margin-top")} + ${window
    //   .getComputedStyle(firstTitle[0])
    //   .getPropertyValue("margin-bottom")})`;

    // sections[i].addEventListener("click", (e) => {
    //   console.log(e);
    //   sections[i].style.maxHeight = "4000px";
    // });
  }
};
