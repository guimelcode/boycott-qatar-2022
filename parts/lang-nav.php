<nav class="lang-nav">
    <ul>
        <?php
        // $all_languages = ['en', 'fr', 'de', 'es'];
        $all_languages = ['en', 'fr', 'de', 'es'];
        foreach ($all_languages as $l): ?>
        <li>
            <?php if ($l == $lang): ?>
                <span class="active">
                    <?=$l?>
                </span>
            <?php else: ?>
                <a href="/<?=$l?>">
                    <?=$l?>
                </a>
            <?php endif?>
        </li>
        <?php endforeach?>
    </ul>
</nav>