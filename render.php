<?php

require_once './convert-markdown.php';

function render($lang)
{
    $contentDir = __DIR__ . '/contenus/' . $lang;
    $contentFiles = array_values(array_diff(scandir($contentDir), array('.', '..', 'imgs')));

    $md = new ConvertMarkdown();
    $rendered = render_header($lang);

    foreach ($contentFiles as $key => $file) {
        if ($key === 1) {

            $rendered .= '<div id="text-content">';
        }

        $data = $md->convert($contentDir . '/' . $file);

        // $outputDir = __DIR__ . '/dist';
        // $outputPath = $outputDir . '/index.html';
        // $rendered .= $data['html'];
        $rendered .= render_template(array(
            'content' => $data['html'],
            'template' => $data['layout'],
            'id' => $data['id'],
            'lang' => $lang,
        ));
        // if($key === 0){
        //     $rendered .= render_lang_nav($lang);
        // }
        if ($key === count($contentFiles) - 1) {

            $rendered .= '</div>';
        }

    }
    echo $rendered;
}

function render_template($locals = array())
{
    extract($locals, EXTR_SKIP);
    ob_start();
    include __DIR__ . '/templates/' . $template . '.tpl.php';
    // echo 'coucouc-------------';
    return ob_get_clean();
}

function render_header($lang)
{
    ob_start();
    include __DIR__ . '/parts/header.php';
    // echo 'coucouc-------------';
    return ob_get_clean();
}
function render_lang_nav($lang)
{
    ob_start();
    include __DIR__ . '/parts/lang-nav.php';
    return ob_get_clean();
}
