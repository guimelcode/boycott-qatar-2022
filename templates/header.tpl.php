<header id="<?=$id?>">
    <div class="front">
        <img src="/assets/svg/url.svg" alt="boycott qatar 2022" class="url"/>
        <img src="/assets/svg/boycott.svg" alt="boycott" class="boycott"/>
        <img src="/assets/svg/url.svg" alt="boycott qatar 2022" class="url"/>
    </div>
    <img src="/assets/svg/qatar-2022.svg" alt="qatar 2022" class="grand-titre"/>
    <picture>
        <source media="(max-width: 384px)" srcset="/assets/ballon/ballon@0,25x.png">
        <source media="(min-width: 385px)" srcset="/assets/ballon/ballon@0,5x.png">
        <source media="(min-width: 1000px)" srcset="/assets/ballon/ballon@0,75x.png">
        <source media="(min-width: 1400px)" srcset="/assets/ballon/ballon.png">
        <img src="/assets/ballon/ballon.png" alt="ballon noir" class="ballon-noir">
    </picture>

    <?=render_lang_nav($lang); ?>

    <div class="content-wrapper">
        <?=$content?>
    </div>
</header>